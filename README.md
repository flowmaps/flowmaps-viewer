# Flowmaps Viewer

This repository is able to recreate a web platform for showing predictions in any layer. Using a configuration file, a geojson and a couple of csv with the predictions and the real data, is able to recreate a website using Vue CLI framework.


## Project configuration
The next files are required for building the website, and have to be stored in the `build_instructions` folder:

- **Geojson file**: A geojson file is required. It has to be on the typical format:
```py
{
    "type":"FeatureCollection", 
    "features": [
        {"type":"Feature","id": "34", "geometry":{"type":"MultiPolygon","coordinates":[], "properties": {"name": "test"}},
    ]
}
```

- **A csv file with the real data**: A csv  (comma separated) is required with the real data. The csv has to have the following mandatory columns:
    + patchId: Corresponding id with the geojson
    + date: The date of the data
    + Any column you want, as well as their error (value + error, value - error) if they have.
    + Please consider the next things about the real data csv:
        * dates sorted upward without holes

- **A csv file with the predictions**: A csv  (comma separated) is required with the predictions. The csv has to have the following mandatory columns:
    + patchId: Corresponding id with the geojson
    + date: The date when the simulation started
    + date_simulation: The date of the data
    + Any column you want, as well as their error (value + error, value - error) if they have.
    + Please consider the next things about the prediction csv:
        * dates can have holes. If you don't have predictions for some dates, it's okey.
        * for a given date and patch, the date_simulation s have to be sorted upward, without holes, starting from "date"

- **A logo for the website**: A svg, png, jpeg, jpg file that will be inserted on the topbar (preferred svg).


Have you put all these files in the `build_instructions` folder? Great! You're ready for the next step, the **config.json** configuration:

At the root of the project, you will find the **config.json** file. In this file, you will let the application know which are the columns, values, errors to use among all the application. So, open the json file and fill it with your files, your desired style of the website. There, you will find a description for each field.

Once you have completed the `config.json` file, it's time to run the tests and see if you have done it well! Run the following commands:
```bash
# The Python command has to be run in the build_instructions/ folder
cd build_instructions
python3 test_static_files.py
```

If all the tests have passed, you're ready for the *building process*:
```bash
# The Python command has to be run in the build_instructions/ folder
cd build_instructions
python3 test_static_files.py
```

If the whole process has worked, you have your app ready to be build with Vue CLI!


## Project setup

Vue CLI is a framework that allows you to create website using Vue in a dedicated environment. First of all, install all the dependencies in the node_modules running the following command:

```
npm install
```

### Compiles and hot-reloads for development
You can run a development server for figuring out if the project is working fine. Just run the next command and a server will be deployed in your machine.
```
npm run serve
```

### Compiles and minifies for production
Once you have tested your app, you can create a production build (js/html/css files) running the following command. A `dist/` folder will be created. Inside, you will find your static web files!
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Code structure
/src/components:
- info: folder refered at the info displayed at clicking each zone
- layout: general visual components among the app as the header, calendar, etc.
- map: components about the geographic map
- ui: base components with styles used among the app

/static:
Stores the static files, as the geojsons, the json with the predictions and the real data.

/store: Based on Store architecture with Vuex.
- index.js: main file with the definition of the store.
- reg_san_obs_09.js: module that provides through api the information about real data
- reg_san_obs_09_predictions.js: module that provides through reading the json file the predictions.
