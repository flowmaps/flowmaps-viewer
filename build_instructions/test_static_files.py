import os
import csv
import unittest
import json
import datetime


with open('../config.json', 'r') as ftx:
    config = json.load(ftx)


class FolderStructureTest(unittest.TestCase):
    """ Test that the project contains the required files. """

    def test_required_files(self):
        """ The next files have to be in build_instructions folder in
        order to build the project. """

        self.assertTrue(os.path.isfile(config['layout']['logo']['file']))
        self.assertTrue(os.path.isfile(config['map']['geojson']['file']))
        self.assertTrue(os.path.isfile(config['real_data']['file']))
        self.assertTrue(os.path.isfile(config['predictions']['file']))


class MapTest(unittest.TestCase):
    """ Test that the geojson is on right format. """
    
    def setUp(self):
        self.geojson = self.get_geojson()
        return super().setUp()
    
    @staticmethod
    def get_geojson():
        with open(config['map']['geojson']['file'], 'r') as ftx:
            geojson = json.load(ftx)
        return geojson
    
    @staticmethod
    def get_geojson_ids():
        geojson = MapTest.get_geojson()
        return [feature['id'] for feature in geojson['features']]
        
    
    def test_geojson_format(self):
        """ Geojson has the right structure """

        self.assertEqual(self.geojson.get('type'), 'FeatureCollection')
        self.assertIsNotNone(self.geojson.get('features', None))
        self.assertEqual(type(self.geojson['features']), list)

        for feature in self.geojson['features']:
            self.assertIsNotNone(feature.get('id', None))
            self.assertIsNotNone(feature.get('type', None))
            self.assertIsNotNone(feature.get('geometry', None))

    def test_geojson_properties_names(self):
        """ Test that each patch on the geojson has the right property
        for the name """

        for feature in self.geojson['features']:
            self.assertIsNotNone(feature.get('properties', None))
            self.assertIsNotNone(feature['properties'].get(config['map']['geojson']['name_label'], None))



class RealDataTest(unittest.TestCase):
    """ Test that the real data csv is on right format and accomplishes the 
    basic premisses """

    def setUp(self):
        self.headers, self.docs = self.get_headers_and_docs()
        return super().setUp()
    
    @staticmethod
    def get_headers_and_docs():
        with open(config['real_data']['file'], 'r') as ftx:
            reader = csv.reader(ftx, delimiter=',')
            rows = []
            for row in reader:
                rows.append(row)
        
        headers = rows[0]
        docs = []
        for row in rows[1:]:
            doc = {column: row[i] for i, column in enumerate(headers)}
            docs.append(doc)
        return headers, docs
    
    @staticmethod
    def get_first_last_date(docs):
        id_example = docs[0]['patchId']
        docs_example = list(filter(lambda doc: id_example == doc['patchId'], docs))

        first_date = docs_example[0]['date']
        last_date = docs_example[-1]['date']
        first_datetime = datetime.datetime.strptime(first_date, "%Y-%m-%d")
        last_datetime = datetime.datetime.strptime(last_date, "%Y-%m-%d")
        return first_datetime, last_datetime

    
    def test_existing_columns(self):
        """ Test that all the columns indicated in the config appears in the csv,
        as well as the date and the patch id. """

        columns = config['real_data']['columns']
        for column in columns:
            self.assertIn(column, self.headers)
        self.assertIn('date', self.headers)
        self.assertIn('patchId', self.headers)

    
    def test_ids_match_geojson(self):
        """ Test that the patch ids are the same that the ones in the geojson """

        geojson_ids = MapTest.get_geojson_ids()

        doc_ids = set()
        for doc in self.docs:
            doc_ids.add(doc['patchId'])
        
        for doc_id in doc_ids:
            self.assertIn(doc_id, geojson_ids)
        for geojson_id in geojson_ids:
            self.assertIn(geojson_id, doc_ids)

    def test_sorted_dates(self):
        """ The dates has to be sorted upward for each patch. """

        first_datetime, last_datetime = self.get_first_last_date(self.docs)
        self.assertGreater(last_datetime, first_datetime)


    def test_all_dates_without_holes(self):
        """ The real data can not have holes. If you need to introduce holes,
        you will have to adapt the Infobox components in order to create the 
        proper date arrays."""

        first_datetime, last_datetime = self.get_first_last_date(self.docs)
        delta = last_datetime - first_datetime
        date_list = []
        for i in range(delta.days + 1):
            day = first_datetime + datetime.timedelta(days=i)
            date_list.append(day.strftime("%Y-%m-%d"))

        doc_ids = set()
        for doc in self.docs:
            doc_ids.add(doc['patchId'])        

        for patchId in doc_ids:
            docs = list(filter(lambda doc: patchId == doc['patchId'], self.docs))
            self.assertEqual(len(docs), len(date_list))
            self.assertEqual(docs[0]['date'], date_list[0])
            self.assertEqual(docs[-1]['date'], date_list[-1])


class PredictionsDataTest(unittest.TestCase):
    """ Test that the prediction csv is on right format and accomplishes the 
    basic premisses """

    def setUp(self):
        self.headers, self.docs = self.get_headers_and_docs()
        return super().setUp()
    
    @staticmethod
    def get_headers_and_docs():
        with open(config['predictions']['file'], 'r') as ftx:
            reader = csv.reader(ftx, delimiter=',')
            rows = []
            for row in reader:
                rows.append(row)
        
        headers = rows[0]
        docs = []
        for row in rows[1:]:
            doc = {column: row[i] for i, column in enumerate(headers)}
            docs.append(doc)
        return headers, docs
    
    @staticmethod
    def get_first_last_date(docs):
        id_example = docs[0]['patchId']
        docs_example = list(filter(lambda doc: id_example == doc['patchId'], docs))

        first_date = docs_example[0]['date']
        last_date = docs_example[-1]['date']
        first_datetime = datetime.datetime.strptime(first_date, "%Y-%m-%d")
        last_datetime = datetime.datetime.strptime(last_date, "%Y-%m-%d")
        return first_datetime, last_datetime
    
    def test_existing_columns(self):
        """ Test that all the columns indicated in the config appears in the csv,
        as well as the date and the patch id. """

        columns = config['predictions']['columns']
        for column in columns:
            self.assertIn(column, self.headers)
        self.assertIn('date', self.headers)
        self.assertIn('date_simulation', self.headers)
        self.assertIn('patchId', self.headers)
    
    def test_ids_match_geojson(self):
        """ Test that the patch ids are the same that the ones in the geojson """

        geojson_ids = MapTest.get_geojson_ids()

        doc_ids = set()
        for doc in self.docs:
            doc_ids.add(doc['patchId'])
        
        for doc_id in doc_ids:
            self.assertIn(doc_id, geojson_ids)
        for geojson_id in geojson_ids:
            self.assertIn(geojson_id, doc_ids)
    
    def test_sorted_dates(self):
        """ The dates has to be sorted upward for each patch. """

        first_datetime, last_datetime = self.get_first_last_date(self.docs)
        self.assertGreater(last_datetime, first_datetime)
    
    def test_prediction_dates_within_real_data_interval(self):
        """ The prediction dates has to be between the max and min dates of real data """

        first_datetime, last_datetime = self.get_first_last_date(self.docs)
        
        real_data_docs = RealDataTest.get_headers_and_docs()[1]
        first_datetime_real, last_datetime_real = RealDataTest.get_first_last_date(real_data_docs)

        self.assertGreater(first_datetime, first_datetime_real)
        self.assertGreater(last_datetime_real, last_datetime)

    def test_prediction_dates_ordered(self):
        """ For a given date of the prediction start, the predicted days have to be
        sorted, and they have to start from the prediction start, and one day step,
        as many days as desired, without holes. """

        first_datetime, last_datetime = self.get_first_last_date(self.docs)
        delta = last_datetime - first_datetime
        date_list = []
        for i in range(delta.days + 1):
            day = first_datetime + datetime.timedelta(days=i)
            date_list.append(day.strftime("%Y-%m-%d"))

        doc_ids = set()
        for doc in self.docs:
            doc_ids.add(doc['patchId'])        

        for patchId in doc_ids:
            for date in date_list:
                docs = list(filter(lambda doc: patchId == doc['patchId'] and date == doc['date'], self.docs))
                for i, doc in enumerate(docs):
                    expected_simulated_date = datetime.datetime.strptime(date, "%Y-%m-%d") + datetime.timedelta(days=i)
                    self.assertEqual(doc['date_simulation'], expected_simulated_date.strftime("%Y-%m-%d"))


class InfoboxTest(unittest.TestCase):
    """ Test that the infobox configuration is alright. """
    
    def test_default_start_dates_make_sense(self):
        """ Check that the default dates on the infobox make sense
        in terms of the real data and the predictions. """
        
        real_data_docs = RealDataTest.get_headers_and_docs()[1]
        first_datetime_real, last_datetime_real = RealDataTest.get_first_last_date(real_data_docs)

        pred_data_docs = PredictionsDataTest.get_headers_and_docs()[1]
        first_datetime_pred, last_datetime_pred = PredictionsDataTest.get_first_last_date(pred_data_docs)

        start_date = config['infobox']['default_dates']['start_date']
        end_date = config['infobox']['default_dates']['end_date']
        start_datetime = datetime.datetime.strptime(start_date, "%Y-%m-%d")
        end_datetime = datetime.datetime.strptime(end_date, "%Y-%m-%d")

        self.assertGreaterEqual(start_datetime, first_datetime_real)
        self.assertGreaterEqual(last_datetime_pred, end_datetime)


class SinglePlotConfigurationTest(unittest.TestCase):
    """ The main plot is set up correctly. """

    def test_existing_columns(self):
        """ Test that the columns that has to be represented exist.
        Axis 2 is not mandatory. """
        
        real_data_columns = config['real_data']['columns']
        pred_data_columns = config['predictions']['columns']

        for axis in ['y_axis_1', 'y_axis_2']:
            if axis == 'y_axis_1':
                self.assertIn(axis, config['infobox']['one_plot_tab'])
            else:
                if axis not in config['infobox']['one_plot_tab']:
                    # Axis 2 is not mandatory
                    continue
    
            for column in config['infobox']['one_plot_tab'][axis]:
                self.assertIn(column['name'], real_data_columns)

                if 'error_min' in column:
                    self.assertIn(column['error_min'], real_data_columns)
                    self.assertIn(column['error_max'], real_data_columns)
                
                if 'prediction_column' in column:
                    self.assertIn(column['prediction_column'], pred_data_columns)

                    if 'prediction_error_min' in column:
                        self.assertIn(column['prediction_error_min'], pred_data_columns)
                        self.assertIn(column['prediction_error_max'], pred_data_columns)

    def test_column_color(self):
        """ Each column has to have color in the RGB format. """

        for axis in ['y_axis_1', 'y_axis_2']:
            if axis == 'y_axis_2' and axis not in config['infobox']['one_plot_tab']:
                continue
    
            for column in config['infobox']['one_plot_tab'][axis]:
                self.assertIsNotNone(column['color'])
                self.assertEqual(type(column['color']), list)
                self.assertEqual(len(column['color']), 3)
    
    def test_existing_verbose(self):
        """ Each column has to be a verbose label indicated. """

        for axis in ['y_axis_1', 'y_axis_2']:
            if axis == 'y_axis_2' and axis not in config['infobox']['one_plot_tab']:
                continue
    
            for column in config['infobox']['one_plot_tab'][axis]:
                self.assertIn(column['name'], config['real_date']['verbose_mapping'])

                if 'prediction_column' in column:
                    self.assertIn(column['prediction_column'], config['predictions']['verbose_mapping'])


class MultiPlotConfigurationTest(unittest.TestCase):
    """ The multiple plot is set up correctly. """

    def test_existing_columns(self):
        """ Test that the columns that has to be represented exist. """

        real_data_columns = config['real_data']['columns']
        pred_data_columns = config['predictions']['columns']

        for plot in config['infobox']['multi_plot_tab']['plots']:
            for axis in ['y_axis_1', 'y_axis_2']:
                if axis == 'y_axis_1':
                    self.assertIn(axis, plot)
                else:
                    if axis not in plot:
                        # Axis 2 is not mandatory
                        continue
        
                for column in plot[axis]:
                    self.assertIn(column['name'], real_data_columns)

                    if 'error_min' in column:
                        self.assertIn(column['error_min'], real_data_columns)
                        self.assertIn(column['error_max'], real_data_columns)
                    
                    if 'prediction_column' in column:
                        self.assertIn(column['prediction_column'], pred_data_columns)

                        if 'prediction_error_min' in column:
                            self.assertIn(column['prediction_error_min'], pred_data_columns)
                            self.assertIn(column['prediction_error_max'], pred_data_columns)
        
    
    def test_row_column_values(self):
        """ In order to attach the plots, the number of plots
        has to be equal than the product of row x columns. """

        rows = config['infobox']['multi_plot_tab']['rows']
        columns = config['infobox']['multi_plot_tab']['columns']
        n_plots = len(config['infobox']['multi_plot_tab']['plots'])
        self.assertEqual(rows * columns, n_plots)
    
    def test_column_color(self):
        """ Each column has to have a name that matches the csv and 
        the color in the RGB format. """

        for plot in config['infobox']['multi_plot_tab']['plots']:
            for axis in ['y_axis_1', 'y_axis_2']:
                if axis == 'y_axis_2' and axis not in plot:
                    continue
        
                for column in plot[axis]:
                    self.assertIsNotNone(column['color'])
                    self.assertEqual(type(column['color']), list)
                    self.assertEqual(len(column['color']), 3)
    
    def test_existing_verbose(self):
        """ Each column has to be a verbose label indicated. """

        for plot in config['infobox']['multi_plot_tab']['plots']:
            for axis in ['y_axis_1', 'y_axis_2']:
                if axis == 'y_axis_2' and axis not in plot:
                    continue
        
                for column in plot[axis]:
                    self.assertIn(column['name'], config['real_date']['verbose_mapping'])

                    if 'prediction_column' in column:
                        self.assertIn(column['prediction_column'], config['predictions']['verbose_mapping'])


if __name__ == '__main__':
    unittest.main()