import csv
import json
import shutil


def generate_required_files():
    with open('../config.json', 'r') as ftx:
        config = json.load(ftx)

    assert generate_json_files(config), "Json files don't created properly."
    assert copy_geojson(config), "Geojson doesn't copied properly."
    assert copy_logo(config), "Logo don't copied propertly."


def generate_json_files(config):
    """
    Open the csv files and create a json with the name `real_data.json`
    and `predictions_data.json` in static folder in order to speed up
    the website.
    """

    real_data_file_csv = config['real_data']['file']
    real_data_file_json = '../src/static/real_data.json'

    predictions_file_csv = config['predictions']['file']
    predictions_file_json = '../src/static/predictions_data.json'

    # Real data
    with open(real_data_file_csv, 'r') as ftx:
        reader = csv.reader(ftx, delimiter=',')
        rows = []
        for row in reader:
            rows.append(row)
    
    docs = []
    headers = rows[0]
    for row in rows[1:]:
        doc = {column: row[i] for i, column in enumerate(headers)}
        docs.append(doc)
    with open(real_data_file_json, 'w') as ftx:
        json.dump(docs, ftx)

    # Predictions
    with open(predictions_file_csv, 'r') as ftx:
        reader = csv.reader(ftx, delimiter=',')
        rows = []
        for row in reader:
            rows.append(row)
    
    docs = []
    headers = rows[0]
    for row in rows[1:]:
        doc = {column: row[i] for i, column in enumerate(headers)}
        docs.append(doc)
    with open(predictions_file_json, 'w') as ftx:
        json.dump(docs, ftx)
    
    return True

def copy_geojson(config):
    name = config['map']['geojson']['file']
    current_path = name
    desired_path = '../src/static/' + name
    shutil.copyfile(current_path, desired_path)
    return True

def copy_logo(config):
    current_path = config['layout']['logo']['file']
    desired_path = '../src/assets/logo.svg'
    shutil.copyfile(current_path, desired_path)
    return True


if __name__ == '__main__':
    generate_required_files()
    print("Succesfully created build the static files!")