import config from '../../../config.json';


function get_locations_from_geojson(geojson) {
    let locations = [];
    geojson.features.forEach((patch) => {
        locations.push(patch.id)
    })
    return locations;
}

function get_names_from_geojson(geojson) {
    let names = [];
    geojson.features.forEach((patch) => {
        names.push(patch.properties[config['map']['geojson']['name_label']])
    })
    return names;
}

export default { 
    get_locations_from_geojson,
    get_names_from_geojson,
};