import GenericTools from '../generic_tools.js';
import config from '../../config.json';

import realData from '../static/real_data.json';

export default {
    namespaced: true,
    state() {
        /*  
            Each value has the following structure:
                variable = {
                    patchId1: [...],
                    patchId2: [...],
                    
                }
                The array of values has as values as days as
                days between firstDate and lastDate.
        */
        var data_object = {}
        for (const column of config['real_data']['columns']) {
            data_object[column] = {};
        }
        return data_object;
    },
    mutations: {
        storeArray(state, payload) {
            const {patchId, field, array} = payload;
            let newField = {...state[field] || {}};
            newField[patchId] = array;
            state[field] = newField;
        }
    },
    actions: {
        loadPatchData(context, payload) {
            const {patchId} = payload;

            if (patchId in context.state[config['real_data']['columns'][0]]) {
                // If the patch is already fetched, don't do it again
                return
            }

            const min_date = context.rootGetters.minDate;
            const max_date = context.rootGetters.maxDate;
            let dateArray = GenericTools.getDates(min_date, max_date);

            var data_by_date = {}
            for (const column of config['real_data']['columns']) {
                data_by_date[column] = [];
            }

            for (const date of dateArray) {
                const docs = realData.filter(doc => doc.date == date && doc.patchId == patchId);
                if (docs && docs.length == 1) {
                    const doc = docs[0]
                    for (const column of config['real_data']['columns']) {
                        data_by_date[column].push(doc[column]);
                    }
                } else {
                    console.log("Problem with real data. Code 01.")
                }
            }

            Object.keys(data_by_date).forEach(column => {
                context.commit('storeArray', {
                    patchId: patchId, 
                    field: column, 
                    array: data_by_date[column],
                })
            });
        }
    },
    getters: {
        data(state) {
            return state;
        }
    },
};