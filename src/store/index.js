import Vuex from 'vuex';
import Vue from 'vue';

import realDataModule from './real_data.js';
import predictionsModule from './predictions';

import realData from '../static/real_data.json';
import predictionsData from '../static/predictions_data.json';


Vue.use(Vuex);


const store = new Vuex.Store({
    modules: {
        "realData": realDataModule,
        "predictionsData": predictionsModule,
    },
    state() {
        /*
        const today = new Date();
        const max_date = today.getFullYear() + '-' + ("0" + (today.getMonth() + 1)).slice(-2) + '-'+today.getDate()
        const min_date = GenericTools.sumDaysToDate(max_date, 365, true)
        */
        return {
            selectedLayer: null,
            selectedPatch: {
                id: null,
                name: null,
            },
            min_date: null,
            max_date: null,
            pred_min_date: null,
            pred_max_date: null,
        };
    },
    mutations: {
        changeMaxMinDates(state, payload) {
            state.min_date = payload['min_date']
            state.max_date = payload['max_date']
            state.pred_min_date = payload['pred_min_date']
            state.pred_max_date = payload['pred_max_date']
        },
        changeSelectedPatch(state, payload) {
            state.selectedPatch = {
                id: payload.id,
                name: payload.name,
            };
        },
        unselectPatch(state) {
            state.selectedPatch = {
                id: null,
                name: null,
            }
        },
    },
    actions: {
        selectPatch(context, payload) {
            context.commit('changeSelectedPatch', payload)
        },
        setMinMaxDates(context) {
            const first_record = realData[0]
            const patchId = first_record.patchId
            const filtered_docs = realData.filter(doc => doc.patchId == patchId)
            const first_date = first_record.date
            const last_date = filtered_docs[filtered_docs.length - 1].date
            
            const pred_first_record = predictionsData[0]
            const pred_patchId = pred_first_record.patchId
            const pred_filtered_docs = predictionsData.filter(doc => doc.patchId == pred_patchId)
            const pred_first_date = pred_first_record.date
            const pred_last_date = pred_filtered_docs[pred_filtered_docs.length - 1].date
            
            context.commit('changeMaxMinDates', {
                min_date: first_date,
                max_date: last_date,
                pred_min_date: pred_first_date,
                pred_max_date: pred_last_date,
            })
        }
    },
    getters: {
        selectedPatchId(state) {
            return state.selectedPatch.id;
        },
        selectedPatchName(state) {
            return state.selectedPatch.name;
        },
        maxDate(state) {
            return state.max_date;
        },
        minDate(state) {
            return state.min_date;
        },
        predMaxDate(state) {
            return state.pred_max_date;
        },
        predMinDate(state) {
            return state.pred_min_date;
        },
    }
});

export default store;