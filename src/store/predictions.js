import GenericTools from '../generic_tools.js';
import config from '../../config.json';
import predictionsData from '../static/predictions_data.json';

export default {
    namespaced: true,
    state() {  
        /*
        Each value has the following structure:
            variable = {
                patchId1: {
                    '2020-04-13': [...]
                },        
            }
            The array of values has the simulation results
            starting from the day of the object key.
        */
        var data_object = {}
        for (const column of config['predictions']['columns']) {
            data_object[column] = {};
        }
        return data_object;
    },
    mutations: {
        storeDict(state, payload) {
            const {patchId, field, dict} = payload;
            let newField = {...state[field] || {}};
            newField[patchId] = dict;
            state[field] = newField;
        }
    },
    actions: {
        loadPatchData(context, payload) {
            const {patchId} = payload;

            if (patchId in context.state[config['predictions']['columns'][0]]) {
                // If the patch is already fetched, don't do it again
                return
            }

            const min_date = context.rootGetters.predMinDate;
            const max_date = context.rootGetters.predMaxDate;
            let dateArray = GenericTools.getDates(min_date, max_date);

            var data_by_date = {}
            for (const column of config['predictions']['columns']) {
                data_by_date[column] = {};
            }
            
            for (const date of dateArray) {
                const docs = predictionsData.filter(doc => doc.date == date && doc.patchId == patchId.toString());
                if (docs && docs.length > 0) {
                    for (const column of config['predictions']['columns']) {
                        data_by_date[column][date] = [];
                    }

                    for (const doc of docs) {
                        for (const column of config['predictions']['columns']) {
                            data_by_date[column][date].push(doc[column]);
                        }
                    }
                }
            }

            Object.keys(data_by_date).forEach(column => {
                context.commit('storeDict', {
                    patchId: patchId, 
                    field: column, 
                    dict: data_by_date[column],
                })
            });
        }
    },
    getters: {
        data(state) {
            return state;
        }
    },
};