import Vue from 'vue';
import App from './App.vue';
import VCalendar from 'v-calendar';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import store from './store/index.js';
import BaseCard from './components/ui/BaseCard.vue';
import TitleWrapper from './components/ui/TitleWrapper.vue';
import ButtonWrapper from './components/ui/ButtonWrapper.vue';

Vue.config.productionTip = false;

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VCalendar)

Vue.component('base-card', BaseCard)
Vue.component('title-wrapper', TitleWrapper)
Vue.component('button-wrapper', ButtonWrapper)

const app = new Vue({
  render: h => h(App),
  store: store,
});

app.$mount('#app');
