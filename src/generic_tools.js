Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function getDates(min_date, max_date) {
    var start = new Date(min_date);
    var end = new Date(max_date);

    // Avoid time change in the date span (e.g. switch from summer time/DST to standard time).
    start.setHours(12,0,0,0);  
    end.setHours(12,0,0,0);
    
    for (var arr=[],dt=new Date(start); dt<=end; dt.setDate(dt.getDate()+1)) {
        arr.push(new Date(dt));
    }
    return arr.map((v)=>v.toISOString().slice(0,10));
}

function sumDaysToDate(startDate, days, to_past=false) {
    var start_date = new Date(startDate);
    return start_date.addDays(to_past? -days: days).toISOString().slice(0, 10)
}

function slice_array(array, min_date, max_date, start_date, end_date) {
    const dateArray = getDates(min_date, max_date);
    const start_idx = dateArray.indexOf(start_date);
    const end_idx = dateArray.indexOf(end_date);
    return array.slice(start_idx, end_idx + 1);
}

function build_rgb_code_from_list(list, opacity) {
    var string = 'rgba(';
    string += list.join(',')
    if (opacity) {
        string += ',' + opacity.toString();
    } else {
        string += ',1';
    }
    string += ')'
    return string;
}

export default { 
    getDates,
    sumDaysToDate,
    slice_array,
    build_rgb_code_from_list,
};